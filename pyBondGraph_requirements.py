# -*- coding: utf-8 -*-
"""
Created on Sun Oct 19 20:20:25 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
"""

numpy_required_version = '1.9.0'       # minimum required numpy version
sympy_required_version = '0.7.5'       # minimum required sympy version
scipy_required_version = '0.14.0'      # minimum required scipy
matplotlib_required_version = '0.14.0'      # minimum required scipy
pyBondGraph_required_version = '0.0.1' # minimum required PyBondGraph

_requirements=[('numpy',numpy_required_version),
          ('sympy',sympy_required_version),
          ('scipy',scipy_required_version),
          ('matplotlib',matplotlib_required_version),
          ('pyBondGraph',pyBondGraph_required_version)]

_modules=['numpy',
          'sympy',
          'scipy',
          'matplotlib',
          'pyBondGraph']

_versions=[numpy_required_version,
          sympy_required_version,
          scipy_required_version,
          matplotlib_required_version,
          pyBondGraph_required_version]

def call_it(func, value):
    return func(value)

def tryImport(library,version):
    try:
        code = compile(('import ' + library), '<string>', 'exec')
        exec code
        code = compile((library + '__version__ > ' \
             + version), '<string>', 'exec')
        exec code
        if library.__version__ < version:
            errorLabel = "pyBondGraph requires " \
                       + str(library) + " >= " \
                       + str(version)
            ImportError(errorLabel)
        return True
    except ImportError:
        errorLabel = "pyBondGraph requires " \
                   + str(library)
        raise ImportError(errorLabel)
        return False

#----------------------------

def satisfy_requirements(moules=_modules, versions=_versions):
    ''' Check for required versions of modules for pyBondGraph'''
    #__version__numpy__ = '1.9.0'       # minimum required numpy version
    #__version__sympy__ = '0.7.5'       # minimum required sympy version
    #__version__scipy__ = '0.14.0'      # minimum required scipy
    #__version__PyBondGraph__ = u'1.4.0' # minimum required PyBondGraph
    
    
    try:
        import dateutil
    except ImportError:
        raise ImportError("pyBondGraph requires dateutil")
    
    try:
        import PyBondGraph # >= u'1.4.0'
        if PyBondGraph.__version__ < u'1.4.0':
            ImportError("pyBondGraph requires PyBondGraph >= u'1.4.0'")
    
    except ImportError:
        raise ImportError("pyBondGraph requires PyBondGraph")
    
    try:
        import numpy
    except ImportError:
        raise ImportError("pyBondGraph requires numpy")
    
    try:
        import dateutil
    except ImportError:
        raise ImportError("pyBondGraph requires dateutil")
    
    try:
        import dateutil
    except ImportError:
        raise ImportError("pyBondGraph requires dateutil")
    
    try:
        import dateutil
    except ImportError:
        raise ImportError("pyBondGraph requires dateutil")

    return True
    
